#include<string.h>
#include<stdio.h>
#include<stdlib.h>

/*        Color difintion         */
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

int main(int argc, char const *argv[]) {
    int del_flag=0,arg_flag=0;
    if(argc<2){
        printf("No targets\n");
        return 1;
    }
    char comp[400]="gcc ",run[100]="./",del[100]="rm ";
    strcat(run,argv[1]);
    strcat(run," ");
    for(int i=1;i<argc;i++){
        if(strcmp(argv[i],"-c")==0){
            arg_flag=1;
            continue;++i;
        }
        if(strcmp(argv[i],"-d")==0){
            del_flag=1;
            continue;++i;
        }
        if(arg_flag==0){
            strcat(comp,argv[i]);
            strcat(comp,".c ");
        }else if(arg_flag==1){
            strcat(run,argv[i]);
            strcat(run," ");
        }
    }
    strcat(comp,"-o ");
    strcat(comp,argv[1]);
    strcat(del,argv[1]);
    printf("Compiling files : %s...",comp);
    if(system(comp)==1){
        printf(RED"Error."RESET);
        return 1;
    }
    printf(GRN "Finished!\n" RESET);
    printf("Executing binary : %s\n\n",run);
    if(system(run)==1){
        printf("Error running binary file");
        return 1;
    }if(del_flag==1){
        printf(RESET"\nDeleting...");
        if(system(del)>0)
            printf("Error deleting binary file\n");
        else
            printf(GRN "Finished!\n" RESET);
    }
    return 0;
}
